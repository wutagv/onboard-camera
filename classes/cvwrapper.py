import cv2
import numpy as np

# operation parameters
block_size = 31
c_constant = -15
kernel = np.array([[0, 1, 0], [1, 1, 1], [0, 1, 0]])

# values imported from calibration tool
transformation_size = (118, 110)
transformation_matrix = np.matrix([[3.76743003e-01, 1.10026261e+00, -2.42769651e+01],
                                   [-1.11929941e-01, 2.76352881e+00, 8.78782534e+00],
                                   [-1.41356387e-03, 1.94449272e-02, 1.00000000e+00]])


class CVWrapper:
    mid_threshold = 0.9

    def __init__(self, timer):
        self.timer = timer

    def get_roi(self, image):
        self.timer.add_time('Image roi')

        [rows, cols] = image.shape
        img_mid = round(self.mid_threshold * rows / 2)
        res = image[img_mid: rows, 0: cols]

        # self.timer.add_image(res, 'B-roi')
        self.timer.add_time('Draw roi')
        return res

    def remove_perspective(self, img):
        self.timer.add_time('Image transformation')

        res = cv2.warpPerspective(img, transformation_matrix, transformation_size)

        # self.timer.add_image(res, 'C-warped')
        self.timer.add_time('Draw image after transformation')
        return res

    def get_thresholded_img(self, transformed):
        self.timer.add_time('Start image thresholding')

        res = CVWrapper.get_thresholded_img_gaussian(transformed)

        self.timer.add_image(res, 'D-thresholded')
        self.timer.add_time('Image thresholding finished')
        return res

    @staticmethod
    def gray_to_rgb(image):
        return cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)

    @staticmethod
    def bgr_to_gray(image):
        return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    @staticmethod
    def get_thresholded_img_gaussian(image):
        return cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, block_size, c_constant)

    @staticmethod
    def get_thresholded_img_mean(image):
        return cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, block_size, c_constant)

    @staticmethod
    def save_image(image, label):
        cv2.imwrite(label, image)

    @staticmethod
    def get_opened_img(image, iterations=1):
        return cv2.morphologyEx(image, cv2.MORPH_OPEN, kernel, iterations=iterations)

    @staticmethod
    def get_closed_img(image, iterations=1):
        return cv2.morphologyEx(image, cv2.MORPH_CLOSE, kernel, iterations=iterations)
