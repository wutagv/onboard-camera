import time
import requests
from classes.camera import CameraWrapper
from classes.perf import Perf
from classes.cvwrapper import CVWrapper
from classes.algo import Algo

camera = CameraWrapper()
timer = Perf()
cv = CVWrapper(timer)
algo = Algo(timer)


class WUTAGV:
    def __init__(self):
        self.counter = 0
        self.start_time = 0
        self.times = []

    def finish(self):
        print("Exit invoked")
        end_time = time.time()
        average = 0
        for t in self.times:
            average += t
        average /= len(self.times)
        print('Time elapsed: ' + str(end_time - self.start_time) + 's')
        print('Iterations: ' + str(self.counter))
        print('Average time is: ' + str(average) + 's')
        print('Saving images...')
        t1 = time.time()
        timer.save_images()
        t2 = time.time()
        print('Operation took ' + str(t2 - t1) + ' seconds')

    def send_values(self, err):
        url = 'http://localhost:5000/data'
        speed = 145
        data = {
            'error': err,
            'speed': speed,
            'tmp_a': speed,
            'tmp_b': speed,
            'message': 'working'
        }

        res = requests.post(url, json=data)

        print('Status code: ' + str(res.status_code))

    def loop_fn(self):
        timer.add_time('Start')

        # read image
        img = camera.get_gray_img()
        timer.add_image(img, 'A-start-image')
        timer.add_time('Image load')

        # get image roi (full width, lower ~55%) - we need info only about the track
        roi = cv.get_roi(img)

        # perspective warp and log results
        transformed = cv.remove_perspective(roi)

        # get thresholded image
        thresholded = cv.get_thresholded_img(transformed)

        # find nearest obstacle
        angle, distance = algo.get_err_vec(thresholded)

        # calculate aggregate error
        aggregate_error = algo.calculate_aggregate_error(angle, distance)

        # and send it to communications programme
        self.send_values(aggregate_error)

        # display profiling values
        timer.add_time('Finish')
        timer.print_times()

        timer.increment_counter()
        timer.clear_times()

    def loop(self):
        self.start_time = time.time()

        try:
            while True:
                print('-----START-----')
                t1 = time.time()
                self.loop_fn()
                self.counter += 1
                t2 = time.time()

                t = t2 - t1
                self.times.append(t)
                print('Iteration number: ' + str(self.counter))
                print("One iteration took: " + str(t) + " seconds")
                print('----END----')
        except KeyboardInterrupt:
            self.finish()
