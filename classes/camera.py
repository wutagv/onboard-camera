import time
import cv2


class CameraWrapper:
    __width = 384
    __height = 218

    def __init__(self):
        self.camera = cv2.VideoCapture(0)
        if not self.camera.isOpened():
            print("Unable to open the camera")
            quit(-1)

        print("Camera opened successfully")

        self.camera.set(cv2.CAP_PROP_FRAME_WIDTH, self.__width)
        self.camera.set(cv2.CAP_PROP_FRAME_HEIGHT, self.__height)

    def get_img(self):
        print('Getting image from RPi')
        t1 = time.time()
        ret, image = self.camera.read()
        t2 = time.time()
        print('Elapsed time is: ' + str(t2 - t1))

        return image

    def get_gray_img(self):
        img = self.get_img()
        return cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
