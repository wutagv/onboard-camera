import cv2
import math

from classes.cvwrapper import CVWrapper


class Algo:
    # obstacle search parameters
    iteration_jump = 3
    min_distance_from_top = 40
    base_angle = 90
    mid = 0
    height = 0
    width = 0
    a_dot = 0
    b_dot = 0
    c_dot = 0

    def __init__(self, timer):
        self.timer = timer

    def find_obstacle(self, image):
        self.height, self.width = image.shape
        self.mid = round(self.width / 2)
        y = self.height - self.iteration_jump
        while y > 0 and image[y, self.mid] == 0:
            y -= self.iteration_jump

        print("Obstacle found at: ({:d}, {:d})".format(self.mid, y))
        self.timer.add_time('Find obstacle')
        return y

    def calc_vector(self, image, y):
        b_x = self.mid + 5
        c_x = self.mid - 5
        start_y = min(self.height - 1, y + 20)
        end_y = max(0, y - 20)

        # set starting values
        b_y = start_y
        c_y = start_y

        # find B and C y values
        while b_y > end_y and image[b_y, b_x] == 0:
            b_y -= 1

        while c_y > end_y and image[c_y, c_x] == 0:
            c_y -= 1

        self.a_dot = (self.mid, y)
        self.b_dot = (b_x, b_y)
        self.c_dot = (c_x, c_y)
        self.timer.add_time('Calculate helper points')

        # compare which is closer and create vec
        if b_y == c_y:
            vec_start = (0, 0)
            vec_end = (0, 1)
        elif b_y > c_y:
            vec_start = self.b_dot
            vec_end = self.c_dot
        else:
            vec_start = self.c_dot
            vec_end = self.b_dot

        return vec_start, vec_end

    def draw_results(self, rgb_img, angle_error):
        # A - red, B - blue, C - green
        red = (255, 0, 0)
        green = (0, 255, 0)
        blue = (0, 0, 255)
        cv2.circle(rgb_img, self.a_dot, 2, red, -1)
        cv2.circle(rgb_img, self.b_dot, 2, blue, -1)
        cv2.circle(rgb_img, self.c_dot, 2, green, -1)

        # add angle info to image
        font = cv2.FONT_HERSHEY_PLAIN
        cv2.putText(rgb_img, '{:.2f}'.format(angle_error), (0, 15), font, 1, red)

    def get_err_vec(self, image):
        y = self.find_obstacle(image)

        rgb_img = CVWrapper.gray_to_rgb(image)

        # no point in drawing dots if no line found
        if y < self.min_distance_from_top:
            return 0, self.height

        vec_start, vec_end = self.calc_vector(image, y)

        # draw line
        purple = (255, 0, 255)
        cv2.arrowedLine(rgb_img, vec_start, vec_end, purple, 2)

        # vec parameters:
        vec = (vec_end[0] - vec_start[0], - vec_end[1] + vec_start[1])
        print('---------')
        print(
            'B: ({:d}, {:d}), C: ({:d}, {:d}), vec: ({:d}, {:d})'.format(self.b_dot[0], self.b_dot[1],
                                                                         self.c_dot[0], self.c_dot[1],
                                                                         vec[0], vec[1])
        )

        angle_rads = math.atan2(vec[1], vec[0])
        angle_degs = math.degrees(angle_rads)
        angle_error = self.base_angle - angle_degs
        print('Error angle is: {:f}'.format(angle_error))
        print('---------')

        self.draw_results(rgb_img, angle_error)

        self.timer.add_image(rgb_img, 'E-vector')
        self.timer.add_time('Determine direction vector')

        distance = self.height - y

        return angle_error, distance

    def calculate_aggregate_error(self, angle, distance):
        calculated_height = self.height - self.min_distance_from_top
        modifier = 2 - (2 * distance / calculated_height)
        aggregated = round(-1 * angle * modifier)
        tmp = max(-90, min(90, aggregated))
        normalized = max(0, min(255, round((tmp + 90) * 255/190)))

        return normalized
