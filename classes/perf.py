import time
import os

from classes.cvwrapper import CVWrapper


class Perf:
    times = []
    images = []
    counter = 0
    name = 'output/image-{:d}-{:s}.png'

    def __init__(self):
        self.debug = os.getenv('DEBUG', False) == 'True'
        print('Debug value: ' + str(self.debug))

    def increment_counter(self):
        self.counter += 1

    def add_time(self, label):
        self.times.append((label, time.time()))

    def add_image(self, image, title):
        if not self.debug:
            return

        label = self.name.format(self.counter, title)
        self.images.append((image, label))

    def print_times(self):
        print('---start---')
        for i in range(1, len(self.times)):
            t = self.times[i][1] - self.times[i - 1][1]
            print('{:s}: {:f}s'.format(self.times[i][0], t))

    def clear_times(self):
        self.times = []

    def save_images(self):
        if not self.debug:
            return

        for img in self.images:
            CVWrapper.save_image(img[0], img[1])
