from classes.wutagv import WUTAGV

import cv2

wutagv = WUTAGV()


def main():
    print('Starting image processing application for WUTAGV')
    print('OpenCV version is: ' + cv2.__version__)
    wutagv.loop()


if __name__ == '__main__':
    main()
