import cv2
import numpy as np
from matplotlib import pyplot as plt


filename = 'calibration/photo_compressed_roi.jpg'

# src corner points
A = np.array([84, 6])
B = np.array([38, 110])
C = np.array([262, 8])
D = np.array([372, 98])

# dst corner points
Adst = np.array([14, 16])
Bdst = np.array([36, 100])
Cdst = np.array([106, 2])
Ddst = np.array([94, 100])


def read_img():
    return cv2.imread(filename, cv2.IMREAD_GRAYSCALE)


def display_images(img, dst):
    plt.subplot(121), plt.imshow(img, 'gray'), plt.title('Input')
    plt.subplot(122), plt.imshow(dst, 'gray'), plt.title('Output')
    plt.show()


def transform_image(img):
    pts1 = np.float32([A, B, C, D])
    pts2 = np.float32([Adst, Bdst, Cdst, Ddst])

    m = cv2.getPerspectiveTransform(pts1, pts2)
    print(np.matrix(m))
    return cv2.warpPerspective(img, m, (118, 110))


def calibrate_camera():
    img = read_img()

    dst = transform_image(img)

    display_images(img, dst)


def main():
    print('Starting image calibration application for WUTAGV')

    print('OpenCV version is: ' + cv2.__version__)
    calibrate_camera()


if __name__ == '__main__':
    main()
