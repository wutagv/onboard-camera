import time

import cv2

from classes.camera import CameraWrapper
from classes.cvwrapper import CVWrapper

camera = CameraWrapper()
counter = 0

while True:
    t1 = time.time()
    image = camera.get_img()
    imageGray = CVWrapper.bgr_to_gray(image)

    cv2.imwrite('camera/test-' + str(counter) + '.jpg', image)
    cv2.imwrite('camera/test-gray-' + str(counter) + '.jpg', imageGray)

    counter += 1

    t2 = time.time()

    print('Time elapsed for one img read operation: ' + str(t2 - t1))

    time.sleep(1)
