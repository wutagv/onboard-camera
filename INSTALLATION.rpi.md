# RPi requirements

### System setup
How to prepare a headless RPi installation:
https://raspberrypi.stackexchange.com/a/57023

### Additional software

The following tools need to be installed on a standard `Raspbian Lite` distro.

Utility software:
```
sudo apt-get install git vim
```

Packages needed for the program to work:
```
sudo apt-get install python3 python3-pip python3-picamera python3-matplotlib python3-numpy
```

### OpenCV 3
http://pklab.net/?id=392&lang=EN

https://github.com/TechBubbleTechnologies/IoT-JumpWay-RPI-Examples/blob/master/_DOCS/2-Installing-OpenCV-3-2-0.md

Install OpenCV 3 with Python 3 bindings:

first - dependencies:
```
sudo apt-get update
sudo apt-get upgrade
```
Build tools:
```
sudo apt-get install build-essential cmake cmake-curses-gui pkg-config
```
image IO packages:
```
sudo apt-get install libjpeg-dev libtiff5-dev libjasper-dev libpng12-dev
```
video IO packages:
```
sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev
sudo apt-get install libxvidcore-dev libx264-dev
```
gtk library (not needed?):
```
// sudo apt-get install libgtk2.0-dev
```
v4l (camera kernel driver) library:
```
sudo apt-get install libv4l-dev v4l-utils
```
Matrix operations libraries:
```
sudo apt-get install libatlas-base-dev gfortran
```
Python header files:
```
sudo apt-get install python3-dev 
```

