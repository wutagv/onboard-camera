notatki:

###Przygotowanie platformy macOS do devu

http://www.pyimagesearch.com/2016/12/19/install-opencv-3-on-macos-with-homebrew-the-easy-way/

potrzebny jest homebrew.


Najpierw instalujemy pythona 3:

```
brew install python3
```

Następnie podpinamy się pod pakiet science i instalujemy opencv3
z powiązaniami do pythona3 (``--with-python3``) i pakietem dodatkowych funkcji (``--with-contrib``):
```
brew tap homebrew/science
brew install opencv3 --with-contrib --with-python3
```

Następnie, po udanej instalacji, należy zmienić nazwę biblioteki powiązań opencv i pythona3:

```
cd /usr/local/opt/opencv3/lib/python3.6/site-packages/
mv cv2.cpython-36m-darwin.so cv2.so
```

Oraz dodać "link" do katalogu w którym znajduje się powyższy pakiet, tak aby python wiedział skąd go importować.
```
echo /usr/local/opt/opencv3/lib/python3.6/site-packages >> /usr/local/lib/python3.6/site-packages/opencv3.pth
```

Na koniec sprawdzamy czy wszystko jest git, odpalamy pythona:
```bash
python3
```

i następnie wpisujemy w konsoli:
```python
import cv2
cv2.__version__
```

Powinna się wyświetlić wersja zgodna z zainstalowaną, na chwilę pisania tego tekstu jest to wersja 3.2.0
