# WUTAGV Onboard Camera 

This project is dedicated to the WUTAGV onboard camera image processing algorithm.

---

The algorithm is written using OpenCV 3 with Python 3.


tutorials:
http://docs.opencv.org/3.2.0/d6/d00/tutorial_py_root.html

### installation

```
pip3 install requests
```
